//
//  AppDelegate.m
//  YMFaceDetectDemo
//
//  Created by reyzhang on 2022/7/25.
//

#import "AppDelegate.h"
#import <YMFaceDetectKit/YMFaceDetectKit.h>
#import "DemoViewController.h"


//译码小二郎 AppKey,AppSecret
#define kYMAppKey @""
#define kYMAppSecret @""

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    //初始化人脸识别
    [[YMFaceDetectEngin shared] registerWithAppKey:kYMAppKey appSecret:kYMAppSecret];
    
    
    
    DemoViewController *demoVC = [[DemoViewController alloc]init];
    UINavigationController *navRoot = [[UINavigationController alloc]initWithRootViewController:demoVC];
    demoVC.title=@"人脸检测";
    self.window.rootViewController = navRoot;
    [self.window makeKeyAndVisible];
    return YES;


    return YES;
}




@end
