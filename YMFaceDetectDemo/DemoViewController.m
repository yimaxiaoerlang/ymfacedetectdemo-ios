//
//  DemoViewController.m
//  YMFaceDetectDemo
//
//  Created by reyzhang on 2022/7/25.
//

#import "DemoViewController.h"

#import <YMFaceDetectKit/YMFaceDetectKit.h>
#import "UIView+ITTAdditions.h"


// UIScreen height.
#define YM_ScreenHeight [UIScreen mainScreen].bounds.size.height

// UIScreen width.
#define YM_ScreenWidth [UIScreen mainScreen].bounds.size.width

// iPhone X
#define  YM_iPhoneX (YM_ScreenWidth == 375.f && YM_ScreenHeight == 812.f ? YES : NO)

// Status bar height.
#define  YM_StatusBarHeight      (YM_iPhoneX ? 44.f : 20.f)

// Navigation bar height.
#define  YM_NavigationBarHeight  44.f

// Tabbar height.
#define  YM_TabbarHeight         (YM_iPhoneX ? (49.f+34.f) : 49.f)

// Tabbar safe bottom margin.
#define  YM_TabbarSafeBottomMargin         (YM_iPhoneX ? 34.f : 0.f)

// Status bar & navigation bar height.
#define  YM_StatusBarAndNavigationBarHeight  (YM_iPhoneX ? 88.f : 64.f)

#define YM_ViewSafeAreInsets(view) ({UIEdgeInsets insets; if(@available(iOS 11.0, *)) {insets = view.safeAreaInsets;} else {insets = UIEdgeInsetsZero;} insets;})



@interface DemoViewController ()
/**
 人脸识别
 */
@property(nonatomic,strong) UIButton *trainButton;
/**
 活体检测
 */
@property(nonatomic,strong) UIButton *detectButton;
@end

@implementation DemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _trainButton = [[UIButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-150)/2, YM_StatusBarAndNavigationBarHeight+10+70, 150, 50)];
    _trainButton.backgroundColor = [UIColor blueColor];
    [_trainButton setTitle:@"开始人脸训练" forState:UIControlStateNormal];
    [_trainButton addTarget:self action:@selector(beginTrain:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_trainButton];
 
    
    
    _detectButton = [[UIButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-150)/2, YM_StatusBarAndNavigationBarHeight+10+70+ 100, 150, 50)];
    _detectButton.backgroundColor = [UIColor blueColor];
    [_detectButton setTitle:@"开始人脸识别" forState:UIControlStateNormal];
    [_detectButton addTarget:self action:@selector(beginDetect:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_detectButton];
    
    
}



// 活体训练
- (void)beginTrain:(id)sender {

    YMFaceUser *userInfo = [YMFaceUser userWithUserId:@"123" userName:@"zhangsan"];
    [[YMFaceDetectEngin shared] startTrainingWithUserInfo:userInfo
                                               completion:^(YMFaceUser * _Nonnull user, NSError * _Nonnull error) {
        if (!error) {
            NSLog(@"训练成功");
        }
    }];
}


//活体检测
- (void)beginDetect:(id)sender {

    [[YMFaceDetectEngin shared]
     startDetect:^(YMFaceUser * _Nonnull user, NSError * _Nonnull error) {
        if (!error) {
            NSLog(@"人脸识别成功，用户信息：%@",user.userId);
        }
    }];
}





@end
